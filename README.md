# Type 1 Diabetes

The files found in this repository were used to analyzing the genetic factors associated with type 1 diabetes.

The study was split into multiple parts, Firstly a quality check (QC) was performed on the data, then QTL mapping was executed using GEMMA.
Lastly the results from QTL mapping were verified with multivariate mapping.
This was primarily done using R and packages within.
Due to confidentiality only the scripts used are shared and not the data used.

## Authors:
- Lin Chang lin25chang@gmail.com
