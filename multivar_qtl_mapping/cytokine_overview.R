# This script prepares data for the cytokines. Such as creating an overview for multivariate QTL analysis.

# Load libraries
library(xlsx)
library(gridExtra)
library(ComplexHeatmap)
library(circlize)

setwd("~/downloads")

#' Create the overview of stimuli against cytokine  samples to see which variables to use for mapping
#' @param data Cytokine data with a sample per row, and cytokine per column
createOverview <- function(data){
  stimuli <- unique(as.vector(sapply(colnames(data[,-1]), function(x){
    gsub("(.*)_.*_.*_(.*)","\\1" ,x)})))
  
  cytokines <- unique(as.vector(sapply(colnames(data[,-1]), function(x){
    gsub("(.*)_.*_.*_(.*)","\\2" ,x)})))
  
  overview <- matrix(nrow = length(stimuli), ncol= length(cytokines), dimnames = list(stimuli, cytokines))
  annotation <- data.frame(Duration = rep("", length(cytokines)), row.names = cytokines, stringsAsFactors = F)
  # Finding the stimulu and cytokine names, column names must be in stimulus_pbmc_dataset_cytokine format
  for(x in colnames(data[,-1])){
    stimulus <- gsub("(.*)_.*_.*_(.*)","\\1" ,x)
    ctk <- gsub("(.*)_.*_.*_(.*)","\\2" ,x)
    time <- gsub("(.*)_.*_(.*)_(.*)","\\2" ,x)
    
    overview[stimulus,ctk] <- length(data[,x]) - sum(is.na(data[,x]))
    annotation[ctk,] <- time
  }
  return (list(overview, annotation))
  grid.table(overview)
}

# Read input file
cytokine24h           <- read.xlsx("20181109_300DM_24h_7d_Cytokines_Lactate_Masterfile.xlsx", 1, header=T, check.names=F)
rownames(cytokine24h) <- cytokine24h[,"NA"]
colnames(cytokine24h) <- gsub("[ +]", "", colnames(cytokine24h))

cytokine7d           <- read.xlsx("20181109_300DM_24h_7d_Cytokines_Lactate_Masterfile.xlsx", 2, header=T, check.names=F)
rownames(cytokine7d) <- cytokine7d[,"NA"]
colnames(cytokine7d) <- gsub("[ +]", "", colnames(cytokine7d))

## Overview of the cytokine samples
#createOverview(cytokine24h)
#createOverview(cytokine7d)
cytokine  <- cbind(cytokine24h, cytokine7d[,-1])
cytokines <- createOverview(cytokine)

overview <- cytokines[[1]]
overview[is.na(overview)] <- 0

# Remove control
overview <- overview[-which(rownames(overview) %in% c("RPMI","RPMIserum","OxLDLRPMI","C16RPMI")),]

# Make data frame with stimuli and their categories
stimulus  <- data.frame( type= rep("", 21), row.names = rownames(overview), stringsAsFactors = F)
bacteria  <- c("B.Burgdorferi", "BorreliaMix", "C.Burnetiiserum", "E.Coli", "MTB", "S.Aureus", "S.Pneumoniae")
fungi     <- c("C.Albicansconidia", "C.Albicanshyphae","Cryptococserum", "Rhizopusmicrosporus", "Rhizopusoryzae","Cryptococcus")
virus     <- c("Influenzaserum")
ligands   <- c("IMQ","LPS1ng", "LPS100ng", "P3C", "PolyI:C")
nonbac    <- c("C16MSU", "OxLDLLPS")

stimulus[bacteria,] <- "bacteria"
stimulus[fungi,]    <- "fungi"
stimulus[virus,]    <- "virus"
stimulus[ligands,]  <- "TLR ligands"
stimulus[nonbac,]   <- "Nonbacterial stimuli"

Heatmap(overview, row_names_side = "left",
        cluster_columns = F, 
        cluster_rows = F, 
        na_col = "white",
        col = colorRamp2(c(0, 1), c("white", "pink")), 
        rect_gp = gpar(col="black", lwd=1.5), 
        column_names_rot = 0, 
        show_heatmap_legend = F, 
        top_annotation = HeatmapAnnotation(pbmc= rep(1, 10), Duration = anno_block(gp = gpar(fill= 2:4), labels = c("1 day", "7 days")), show_legend = F),
        right_annotation = rowAnnotation(Type = anno_block(gp=gpar(fill = 5:10))),
        column_split = cytokines[[2]], 
        row_split = stimulus, 
        row_title_rot = 0, 
        column_title = NULL, 
        row_title_side = "right",
        column_gap = unit(0, "mm"), 
        row_gap = unit(3, "mm"),
        border=F
)
