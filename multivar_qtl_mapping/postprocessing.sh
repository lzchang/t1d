#!/bin/bash

base_name=Pneumoniae24h
threshold=1e-3

cat /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/gemma/output/0$base_name".assoc.txt" > "complete_"$base_name".txt"

for i in $(seq 1 20)
do
cat /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/gemma/output/$i$base_name".assoc.txt" | tail -n +2 >> "complete_"$base_name".txt"
done

cols=$(awk -F' ' '{print NF; exit}' "complete_"$base_name".txt")
head -1  "complete_"$base_name".txt" > "complete_"$base_name"_"$threshold".txt"
awk -v thres="$threshold" -v col="$cols" '$col <= thres' "complete_"$base_name".txt" >> "complete_"$base_name"_"$threshold".txt"

rm [0-9]*$base_name*
rm $base_name".cXX.txt"
rm $base_name".log.txt"

sbatch compare_manhattan.sh $base_name
