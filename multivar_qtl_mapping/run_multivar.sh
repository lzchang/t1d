#!/bin/bash
#SBATCH --time=72:00:00
#SBATCH --cpus-per-task=4
#SBATCH --mem=50gb
#SBATCH --job-name=multivariate_mapping
#SBATCH --error=multivar_mapping.err

module load GEMMA

# Define variables
maf=0.2
basename=LPS1ng24h

./gemma-0.98.1-linux-static -g genotype_combined_for_relative.txt -p $basename".txt" -gk 1 -o $basename -maf $maf

n_traits=$(awk -F' ' '{print NF; exit}' $basename'.txt')
n=1

for chrom in $(seq 1 $n_traits)
   do
      stringtoprint="$stringtoprint $chrom"
   done

./gemma-0.98.1-linux-static -g $1"geno_split.txt" -p $basename".txt" -k "output/"$basename".cXX.txt" -lmm 1 -n $stringtoprint -o $1$basename

