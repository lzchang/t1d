#!/bin/bash
#SBATCH --time=4:00:00
#SBATCH --cpus-per-task=4
#SBATCH --mem=50gb
#SBATCH --job-name=combinegenotype
#SBATCH --error=preprocess1.err

module load R

echo "Starting splitting phenotype"

Rscript t1d_multitrait_phenotypes.R \
-s /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/gemma/multitrait_t1d_summary.csv \
-p /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/t1d_cytokinelog2.txt

echo "Combined file, see output"
