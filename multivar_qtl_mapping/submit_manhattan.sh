#!/bin/bash
#SBATCH --time=00:45:00
#SBATCH --cpus-per-task=4
#SBATCH --mem=60gb
#SBATCH --job-name=manhattanplot
#SBATCH --error=preprocess.err

module load R

echo "Starting making manhattan plots"

Rscript /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/create_manhattan.R \
-a "complete_"$1"_1e-3.txt" \
-b $1 \
-p /groups/umcg-wijmenga/tmp04/umcg-lchang/locus_zoom/snp_pos_for_mapping.txt \
-o $1"_plot.pdf"

echo "Manhattan plots made, see output files"
