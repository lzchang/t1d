#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH --cpus-per-task=4
#SBATCH --mem=100gb
#SBATCH --job-name=multivariate_mapping
#SBATCH --error=multivar_mapping.err

module load GEMMA

n_split=20

total=$(wc -l < genotype_combined.txt)
chunk_size=$(($((total + 1))/n_split))

for i in $(seq 0 $n_split)
do
# split genotype into chunks
#sed -n "$((chunk_size*i+1)), $(($((chunk_size*i))+chunk_size+1)) p" "genotype_combined.txt" > "$i""geno_split.txt"

sbatch run_multivar.sh $i

done
