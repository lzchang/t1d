# Combine dosage files with the alleles into 1 file to use for GEMMA. Used in the 300DM cohort.
#- Lin Chang

# Load libraries
library(optparse)
library(data.table)

# Define options
options = list(
  make_option(c("-s", "--snp"), action="store", default=NA, type="character",
              help="Path to folder containing SNPs"),
  make_option(c("-p", "--pruned"), action="store", default=NA, type="character",
              help= "Filename of pruned data excluding the HLA region for creating relative matrix"),
  make_option(c("-o", "--output"), action="store", default=NA, type="character",
              help= "Filename indicating the output file"),
  make_option(c("-l", "--output2"), action="store", default=NA, type="character",
              help= "Filename indicating the output file")
  
)

# Read the command line
parser <- parse_args(OptionParser(option_list = options))

## Name of directory containing SNP files
dir <- parser$snp

## Variable of text file containing SNPS to keep for relative matrix
pruned_file_name <- parser$pruned

## Output file name
output_file_name <- parser$output
output_file_name2 <- parser$output2



# Fix the relative path for matchfile
match_file <- read.table("../dosage_matrix/matchfile.txt", sep="\t", header=T)
samples <- scan("t1d_samples.txt", what="char")

# Samples of T1D cohort to keep
sample_ids <- as.vector(na.omit(match_file$ID.DNA[match_file$Code.Anouk %in% samples]))

combineChr <- function(x){
  ## Obtain filenames of dosage and variantinfo file of chromosome
  snps_location_file_name  <- paste(dir, x, sep="")
  variantInfo_file_name   <- gsub("[.]dosage", ".variantInfo", snps_location_file_name)
   
  ## Read the input files
  variantinfo <- fread(variantInfo_file_name, header=T)
  genotype <- fread(snps_location_file_name, header=T)
  colnames(genotype)[1] <- "rsID"
  colnames(genotype) <- gsub("[0-9]*_", "", colnames(genotype))


  sample_ids <- sample_ids[sample_ids %in% colnames(genotype)]

  genotype <- cbind("rsID"=genotype[,rsID], genotype[,sample_ids, with=FALSE])
  ## Only keep relevant variantinfo columns
  variantinfo <- variantinfo[,c("rsID", "EffectAllele", "AlternativeAllele")]
  
  ## Set key for merge
  setkey(variantinfo, "rsID")
  setkey(genotype, "rsID")
  
  ## merge two files together
  merged <- merge(variantinfo, genotype, by="rsID")
  
  return(merged)
}

#combined <- as.data.frame(do.call("rbind", lapply(list.files(path=dir, pattern = "^300DM.*.dosage"), combineChr)))
#write.table(combined, output_file_name, sep=",", row.names = F, col.names = F, quote=F)

combined <- fread(output_file_name, header=F, sep=",")
colnames(combined)[1] <- "id"
setkey(combined, id)

# Prune the combined data, to keep only SNPs not within HLA region
pruned <- scan(pruned_file_name, what = "char")
combined_pruned <- na.omit(combined[pruned])
# Write combined table to output file
write.table(combined_pruned, output_file_name2, sep=",", row.names = F, col.names = F, quote=F)

