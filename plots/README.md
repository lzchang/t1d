# Visualization

Files here were used for visualizing the results, this includes creating a boxplot and heatmap of the results from QTL mapping.
Uses pheatmap and ggplot2.
