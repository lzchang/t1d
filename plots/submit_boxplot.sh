#!/bin/bash
#SBATCH --time=03:00:00
#SBATCH --cpus-per-task=2
#SBATCH --mem=20gb
#SBATCH --job-name=boxplot
#SBATCH --error=boxplot.err

module load R

echo "Starting making boxplot of genotype..."

Rscript Genotype_boxplot.R \
-s /groups/umcg-wijmenga/tmp04/umcg-lchang/locus_zoom/post_processing_out_ctk/locus_files_5e-08_2/all_top_snps.txt \
-f /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/dosage_matrix \
-g /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/dosage_matrix/t1d_cytokinelog2.txt \
-m /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/dosage_matrix/matchfile.txt \
-o cqtl_multitrait_boxplot.pdf

echo "Genotype boxplot completed, see output"
