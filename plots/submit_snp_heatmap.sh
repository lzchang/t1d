#!/bin/bash
#SBATCH --time=0:45:00
#SBATCH --cpus-per-task=2
#SBATCH --mem=8gb
#SBATCH --job-name=CreateHeatmapSNPvCQTL
#SBATCH --error=heatmap.err

module load R

echo "Reading file..."

Rscript Heatmap_SNP_cQTL.R \
-r /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/mapping_output/result_cytokine.txt \
-s /groups/umcg-wijmenga/tmp04/umcg-lchang/locus_zoom/post_processing_out/locus_files_5e-08_2/all_top_snps.txt \
-o 300DM_cQTLv_300DM_SNP.png

echo "Finished creating heatmap"
