# Quality check

Files found in this directory are used in assessing the quality of the data.
This includes drawing distribution plots, PCA analysis and removing outliers. The main file that was used for tests can be found here too.
This was done in R, using various packages such as xlsx, pheatmap and ggplot2.