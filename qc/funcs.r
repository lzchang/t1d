# Functions are defined here for the QC process, including normalization, drawing histogram and PCA plots.
#- Lin Chang (lin25chang@gmail.com)

# Draws a PCA plot given two components to view outliers
# PCA: PCA object
# label: vector with names for samples
# comp1/comp2: integers defining which components to plot
plot_comp <- function(PCA, label, comp1, comp2) {
  variances <- PCA$sdev ^ 2 / sum(PCA$sdev ^ 2)
  par(oma = c(0, 0, 0, 5))
  plot(
    PCA$x[, comp2] ~ PCA$x[, comp1],
    xlab = paste("component", comp1, "(", 100 * round(variances[comp1], 3), "%)"),
    ylab = paste("component", comp2, "(", 100 * round(variances[comp2], 3), "%)")
  )
  text(
    PCA$x[, comp2] ~ PCA$x[, comp1],
    labels = label,
    pos = 2 ,
    cex = 0.5
  )
  
  abline(v = sd(PCA$x[, comp1]), col = "green")
  abline(h = sd(PCA$x[, comp2]), col = "green")
  
  abline(v = sd(PCA$x[, comp1]) * 2, col = "blue")
  abline(h = sd(PCA$x[, comp2]) * 2, col = "blue")
  
  abline(v = sd(PCA$x[, comp1]) * 3, col = "red")
  abline(h = sd(PCA$x[, comp2]) * 3, col = "red")
  legend(
    par('usr')[2],
    par('usr')[4],
    legend = c("sd1", "sd2", "sd3"),
    col = c("green", "blue", "red"),
    lty = 1,
    lwd = 3,
    bty = 'n',
    xpd = NA
  )
}


# Draws a histogram with a normal distribution line 
# param: name of the phenotype
# vec: vector with the values to plot
drawHistogram <- function(param, vec) {
  hist(
    vec[,param],
    breaks = 50,
    freq = F,
    main = param,
    xlab = "Values (log2)"
  )
  minimum <- min(vec[,param]) * 100
  maximum <- max(vec[,param]) * 100
  lines(minimum:maximum / 100,
        dnorm(
          minimum:maximum / 100,
          mean = mean(vec[,param]),
          sd = sd(vec[,param])
        ),
        col = "red")
}

# Turn a matrix numeric to use in operations such as log2 transformation
# para: matrix to turn numeric
MatrixToNumeric <- function(para) {
  as.numeric(para)
}

# Inverse rank normalization 
# x: data to normalize
inverseRankNorm  <-  function(x){
 res <- rank(x)
 res <- qnorm(res/(length(res)+0.5))

 return(res)
}
