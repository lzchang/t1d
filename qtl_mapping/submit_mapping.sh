#!/bin/bash
#SBATCH --time=5:00:00
#SBATCH --cpus-per-task=4
#SBATCH --mem=60gb
#SBATCH --job-name=qtlmappingt1dcytokine
#SBATCH --error=qtlmapping.err

module load R

echo "Starting QTL mapping..."

Rscript t1d_qtlmapping.R \
-e /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/dosage_matrix/t1d_cytokine7d.txt \
-c /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/dosage_matrix/t1d_covs.txt \
-s /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/dosage_matrix/ \
-p 1e-2 \
-m /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/dosage_matrix/matchfile.txt \
-o /groups/umcg-wijmenga/tmp04/umcg-lchang/T1D/result_cytokine7d.txt

echo "QTL mapping finished, see output"
